class Rules(object):
    def __init__(self):
        self.rules = {
            'Fizzbuzz': Rules.is_fizzbuzz,
            'Buzz': Rules.is_buzz,
            'Fizz': Rules.is_fizz
        }

    def apply(self, number):
        value = number + 1

        for word, function in self.rules.items():
            if function(value) == True:
                value = word
                break
        return value

    def is_fizzbuzz(value):
        if Numbers(value).multiple_of(15):
            return True
        return False

    def is_buzz(value):
        if Numbers(value).multiple_of(5):
            return True
        return False

    def is_fizz(value):
        if Numbers(value).multiple_of(3):
            return True
        return False

class Numbers(object):
    def __init__(self, value):
        self.value = value

    def multiple_of(self, number):
        return self.value % number == 0
