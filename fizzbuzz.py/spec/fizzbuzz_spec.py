from mamba import description, context, it
from expects import expect, equal
from fizzbuzz import Fizzbuzz


with description('Fizzbuzz') as self:
    with it('prints an array length 100'):
        array = Fizzbuzz.print()

        expect(len(array)).to(equal(100))

    with it('return fizz in two position'):
        array = Fizzbuzz.print()

        expect(array[2]).to(equal('Fizz'))

    with it('return buzz in four position'):
        array = Fizzbuzz.print()

        expect(array[4]).to(equal('Buzz'))

    with it('return Fizzbuzz in fourteen position'):
        array = Fizzbuzz.print()

        expect(array[14]).to(equal('Fizzbuzz'))

    with it('return Fizz if number is a muliple of three'):
        array = Fizzbuzz.print()

        expect(array[5]).to(equal('Fizz'))
