from rules import Rules

class Fizzbuzz(object):
    def print():
        result = []
        range_array = range(100)
        
        for number in range_array:
            value = Rules().apply(number)
            result.append(value)

        return result
