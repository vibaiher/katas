# FizzBuzz

Source <http://katayuno-app.herokuapp.com/katas/1>.

## Problem

- List the numbers from 1 to 100.
- If the number is divisible by 3, write "Fizz" instead.
- If the number is divisible by 5, write "Buzz" instead
- If the number is divisible by both 3 and 5, write "FizzBuzz".
- If the number is not divisible neither by 3 nor 5, write the string representation of the number.

## Objectives

- Practice Go language
- Use a BDD testing library

## Bitacora

### Setup

- Create Dockerfile with latets Go version
- Look for most used BDD testing library in awesome-go.com
  - Ginkgo looks great. It uses Gomega as matcher library
- Install it using go get
- Bootstrap ginkgo
- Change workdir so code is in $GOPATH
- Review ginkgo cli options
- Prepare scripts for running a shell or running tests

### Lists the numbers from 1 to 100

- Test FizzBuzz returns a list of 100 elements
- Create an integer array of first 100 numbers. (No range option found)
- Test first element returned is 1. Test was green...
- Test last element returned is 100. Test was green...

### If the number is divisible by 3, write "Fizz" instead

- Test list returns "Fizz" instead of 3
- Syntax error. Test script breaks... Fix it
- Use FizzBuzz2 to avoid breaking FizzBuzz interface
- First time ginkgo is executed is slow. Use shell and use `ginkgo watch` to run tests automatically.
- Finish parallel change
