package fizzbuzz_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	. "fizzbuzz"
)

var _ = Describe("FizzBuzz", func() {
	It("lists all numbers", func() {
		expectedNumbers := 100

		list := FizzBuzz()

		Expect(len(list)).To(Equal(expectedNumbers))
	})

	It("starts from 1", func() {
		expectedNumber := "1"

		list := FizzBuzz()
		firstNumber := list[0]

		Expect(firstNumber).To(Equal(expectedNumber))
	})

	It("ends at 100", func() {
		expectedNumber := "100"

		list := FizzBuzz()
		firstNumber := list[99]

		Expect(firstNumber).To(Equal(expectedNumber))
	})

	It("converts 3 into Fizz", func() {
		fizz := "Fizz"

		list := FizzBuzz()
		three := list[2]

		Expect(three).To(Equal(fizz))
	})
})
