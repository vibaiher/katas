package fizzbuzz

import "strconv"

const fizz = "Fizz"

func FizzBuzz() []string {
	list := createNumbersListOf(100)
	numbers := stringify(list)
	return numbers
}

func createNumbersListOf(lenght int) []int {
	list := make([]int, lenght)
	for index := range list {
		list[index] = index + 1
	}

	return list
}

func stringify(numbers []int) []string {
	list := make([]string, len(numbers))
	for index := range numbers {
		number := numbers[index]

		if number == 3 {
			list[index] = fizz
		} else {
			list[index] = toString(number)
		}
	}

	return list
}

func toString(number int) string {
	return strconv.Itoa(number)
}
