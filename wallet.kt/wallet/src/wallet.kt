package org.wallet.wallet

class Wallet {
  var stocks: ArrayList<String>
    private set

  init {
    stocks = ArrayList<String>()
  }

  fun add(stock: String) {
    stocks.add(stock)
  }
}
