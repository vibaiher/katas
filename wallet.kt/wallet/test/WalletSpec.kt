package org.wallet.wallet

import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.*
import com.winterbe.expekt.expect

object WalletSpec: Spek({
  describe("Wallet") {
    it("contains Stocks") {
      val wallet = Wallet()

      wallet.add("Euros")
      wallet.add("Dollars")

      val expectedStocks = arrayListOf("Euros", "Dollars")
      expect(wallet.stocks).to.equal(expectedStocks)
    }
  }
})
